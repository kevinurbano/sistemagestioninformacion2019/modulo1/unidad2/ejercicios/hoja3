﻿USE ciclistas;
/* Modulo 1 Unidad 2 */

/* Consulta de seleccion 3 */

/* Consulta 1 */
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Banesto';

/* Consulta 2 */
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Banesto' OR c.nomequipo='Navigare';

/* Consulta 3 */
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32;

/* Consulta 4 */
SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto' OR c.edad BETWEEN 25 AND 32;

/* Consulta 5 */
SELECT DISTINCT LEFT(c.nomequipo,1) FROM ciclista c WHERE LEFT(c.nombre,1)='R';

/* Consulta 6 */
SELECT e.numetapa FROM etapa e WHERE e.salida=e.llegada;

/* Consulta 7 */
SELECT e.numetapa FROM etapa e WHERE e.salida<>e.llegada;

/* Consulta 8 */
SELECT DISTINCT p.nompuerto FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;

/* Consulta 9 */
SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;

/* Consulta 10 */
SELECT COUNT(DISTINCT e.dorsal) FROM etapa e;

/* Consulta 11 */
SELECT COUNT(DISTINCT p.numetapa) FROM puerto p;

/* Consulta 12 */
SELECT COUNT(DISTINCT p.dorsal) FROM puerto p;

/* Consulta 13 */
SELECT p.numetapa,COUNT(*) FROM puerto p GROUP BY p.numetapa;

/* Consulta 14 */
SELECT AVG(p.altura) FROM puerto p;

/* Consulta 15 */
SELECT p.numetapa FROM puerto p GROUP BY p.numetapa HAVING AVG(p.altura)>1500;

/* Consulta 16 */
SELECT COUNT(*) totalEtapas 
  FROM 
    (SELECT 
      p.numetapa 
      FROM 
        puerto p 
      GROUP BY 1
        p.numetapa 
      HAVING 
        AVG(p.altura)>1500) c1;

/* Consulta 17 */
SELECT l.dorsal,COUNT(*) llevaMaillot FROM lleva l GROUP BY l.dorsal;

/* Consulta 18 */
SELECT dorsal,código,COUNT(*) FROM lleva GROUP BY dorsal,código;

/* Consulta 19 */
SELECT l.dorsal,
       l.numetapa,
       COUNT(*)
  FROM lleva l GROUP BY l.dorsal,l.numetapa;